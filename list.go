package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func ListScheduledMessages(w http.ResponseWriter, r *http.Request) {
	user, ctx, reqLog := CheckAccessToken(w, r)
	if user == nil {
		return
	}
	vars := mux.Vars(r)
	roomID := vars["roomID"]
	result, err := db.FindMessagesForUserInRoom(ctx, user.UserID, roomID)
	if err != nil {
		reqLog.Errorfln("Failed to get messages in %s for %s from database: %v", roomID, user.UserID, err)
		errGetListFromDatabase.Write(w)
		return
	}
	err = json.NewEncoder(w).Encode(&result)
	if err != nil {
		reqLog.Warnfln("Failed to encode %d messages in %s for %s: %v", len(result), roomID, user.UserID, err)
	} else {
		reqLog.Debugfln("Sent list of %d scheduled messages in %s to %s", len(result), roomID, user.UserID)
	}
}
