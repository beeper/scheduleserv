package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// ErrorResponse wraps a standard Matrix error response.
// It also contains the HTTP status code to use when responding to clients.
type ErrorResponse struct {
	Code    string `json:"errcode"`
	Message string `json:"error"`
	Status  int    `json:"-"`
}

// Write writes the error response to a HTTP client.
func (er ErrorResponse) Write(w http.ResponseWriter) {
	w.WriteHeader(er.Status)
	_ = json.NewEncoder(w).Encode(&er)
}

var (
	errNotFound = ErrorResponse{
		Code:    "M_NOT_FOUND",
		Message: "Couldn't find a scheduled message with that ID",
		Status:  http.StatusNotFound,
	}
	errNoRescheduleSent = ErrorResponse{
		Code:    "COM.BEEPER.NO_RESCHEDULE_SENT",
		Message: "Can't reschedule a message that has already been sent",
		Status:  http.StatusBadRequest,
	}
	errAlreadySending = ErrorResponse{
		Code:    "COM.BEEPER.ALREADY_SENDING",
		Message: "Can't reschedule or cancel a message that is currently being sent",
		Status:  http.StatusConflict,
	}
	errScheduleTooMany = ErrorResponse{
		Code:    "COM.BEEPER.SCHEDULE_TOO_MANY",
		Message: fmt.Sprintf("You can only schedule %d messages in a single room.", MaxScheduleCount),
		Status:  http.StatusBadRequest,
	}
	errScheduleTooFar = ErrorResponse{
		Code:    "COM.BEEPER.SCHEDULE_TOO_FAR",
		Message: "Can't schedule messages more than 1 year into the future",
		Status:  http.StatusBadRequest,
	}
	errSchedulePast = ErrorResponse{
		Code:    "COM.BEEPER.NO_SCHEDULE_PAST",
		Message: "Can't schedule messages in the past",
		Status:  http.StatusBadRequest,
	}
	errRequestNotJSON = ErrorResponse{
		Code:    "M_NOT_JSON",
		Message: "Failed to parse request JSON",
		Status:  http.StatusBadRequest,
	}
	errMismatchingUserID = ErrorResponse{
		Code:    "COM.BEEPER.MISMATCHING_USER_ID",
		Message: "The X-Beeper-User header doesn't match the provided access token",
		Status:  http.StatusForbidden,
	}

	errCheckAccessToken = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to check access token",
		Status:  http.StatusInternalServerError,
	}
	errUpdateUserDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to update user info",
		Status:  http.StatusInternalServerError,
	}
	errGetFromDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to get message from database",
		Status:  http.StatusInternalServerError,
	}
	errGetListFromDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to get messages from database",
		Status:  http.StatusInternalServerError,
	}
	errGetCountFromDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to check number of existing scheduled messages",
		Status:  http.StatusInternalServerError,
	}
	errUpdateDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to update scheduled message",
		Status:  http.StatusInternalServerError,
	}
	errInsertDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to insert message into database",
		Status:  http.StatusInternalServerError,
	}
	errDeleteDatabase = ErrorResponse{
		Code:    "M_UNKNOWN",
		Message: "Failed to delete message from database",
		Status:  http.StatusInternalServerError,
	}
)

func errFieldMissing(field string) ErrorResponse {
	return ErrorResponse{
		Code:    "COM.BEEPER.FIELD_MISSING",
		Message: fmt.Sprintf("The %s field is required", field),
		Status:  http.StatusBadRequest,
	}
}
