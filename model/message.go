package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"

	log "maunium.net/go/maulogger/v2"
)

type ScheduledMessage struct {
	ID    int64  `json:"id"`
	TxnID string `json:"txn_id"`

	RoomID    string  `json:"room_id"`
	UserID    string  `json:"-"`
	EventType string  `json:"type"`
	StateKey  *string `json:"state_key,omitempty"`

	TargetTimestamp int64  `json:"target_timestamp"`
	SentTimestamp   *int64 `json:"sent_timestamp,omitempty"`
	ResponseStatus  int    `json:"response_status,omitempty"`

	Content  map[string]interface{} `json:"content"`
	Response map[string]interface{} `json:"response,omitempty"`

	// Only present when getting upcoming messages, not sent back to the user
	UserAccessToken string `json:"-"`
}

func (sm *ScheduledMessage) makeURL(baseURL string) string {
	endpoint := path.Join("_matrix", "client", "r0", "rooms", url.PathEscape(sm.RoomID))
	if sm.StateKey != nil {
		endpoint = path.Join(endpoint, "state", sm.EventType, *sm.StateKey)
	} else {
		endpoint = path.Join(endpoint, "send", sm.EventType, fmt.Sprintf("scheduleserv_%d", sm.ID))
	}
	return fmt.Sprintf("%s/%s", baseURL, endpoint)
}

const scheduledKey = "com.beeper.scheduled"

func (sm *ScheduledMessage) contentReader() (io.Reader, error) {
	var buf bytes.Buffer
	sm.Content[scheduledKey] = true
	return &buf, json.NewEncoder(&buf).Encode(&sm.Content)
}

func (sm *ScheduledMessage) prepareRequest(baseURL string) (*http.Request, error) {
	content, err := sm.contentReader()
	if err != nil {
		return nil, fmt.Errorf("failed to encode content: %w", err)
	}

	req, err := http.NewRequest(http.MethodPut, sm.makeURL(baseURL), content)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", sm.UserAccessToken))
	return req, nil
}

var sendLog = log.Sub("Send")

func (sm *ScheduledMessage) Send(client *http.Client, baseURL string) error {
	req, err := sm.prepareRequest(baseURL)
	if err != nil {
		return fmt.Errorf("failed to create request: %w", err)
	}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send request: %w", err)
	}
	defer resp.Body.Close()
	sm.ResponseStatus = resp.StatusCode
	err = json.NewDecoder(resp.Body).Decode(&sm.Response)
	if err != nil {
		sendLog.Warnfln("Failed to decode response content from sending %d: %v", sm.ID, err)
	}
	return nil
}
