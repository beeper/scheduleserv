package model

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

type User struct {
	UserID      string
	AccessToken string
}

type WhoamiResponse struct {
	UserID  string `json:"user_id,omitempty"`
	ErrCode string `json:"errcode,omitempty"`
	Error   string `json:"error,omitempty"`
	Status  int    `json:"-"`
}

func (user *User) CheckAccessToken(ctx context.Context, client *http.Client, baseURL string) (*WhoamiResponse, error) {
	url := fmt.Sprintf("%s/_matrix/client/r0/account/whoami", baseURL)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create request: %w", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", user.AccessToken))
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to send request: %w", err)
	}
	defer resp.Body.Close()
	var respBody WhoamiResponse
	err = json.NewDecoder(resp.Body).Decode(&respBody)
	if err != nil {
		return nil, fmt.Errorf("failed to decode response body: %w", err)
	}
	respBody.Status = resp.StatusCode
	return &respBody, nil
}
