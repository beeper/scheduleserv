package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/beeper/scheduleserv/model"
)

func ScheduleMessage(w http.ResponseWriter, r *http.Request) {
	user, ctx, reqLog := CheckAccessToken(w, r)
	if user == nil {
		return
	}

	var msg model.ScheduledMessage
	if !parseJSON(w, r, &msg) ||
		!fieldCheck(w, len(msg.RoomID) > 0, "room_id") ||
		!fieldCheck(w, len(msg.EventType) > 0, "type") ||
		!fieldCheck(w, msg.Content != nil, "content") ||
		!validateTargetTimestamp(w, msg.TargetTimestamp) {
		reqLog.Debugfln("Rejecting scheduled message from %s because the request is missing fields", user.UserID)
		return
	}
	vars := mux.Vars(r)
	msg.TxnID = vars["txnID"]
	msg.UserID = user.UserID

	existingMessage, err := db.GetMessageByTxnID(ctx, msg.TxnID, msg.UserID, msg.RoomID, msg.EventType)
	if err != nil {
		reqLog.Warnfln("Failed to check for existing messages with transaction ID %s from %s: %v", msg.TxnID, msg.UserID, err)
	} else if existingMessage != nil {
		// The user already scheduled a message with the same transaction ID, return that one.
		err = json.NewEncoder(w).Encode(existingMessage)
		if err != nil {
			reqLog.Warnfln("Failed to encode message %d in response to %s (transaction ID dedup): %v", existingMessage.ID, user.UserID, err)
		} else {
			reqLog.Debugfln("Sent existing scheduled message %d in response to %s after transaction ID deduplication", existingMessage.ID, user.UserID)
		}
		return
	}

	count, err := db.CountMessagesForUserInRoom(ctx, msg.UserID, msg.RoomID)
	if err != nil {
		reqLog.Errorfln("Failed to check number of existing scheduled messages %s has: %v", user.UserID, err)
		errGetCountFromDatabase.Write(w)
		return
	}
	if count > MaxScheduleCount {
		reqLog.Debugfln("Rejecting scheduled message from %s because they have %d messages already scheduled in that room", user.UserID, count)
		errScheduleTooMany.Write(w)
		return
	}

	err = db.InsertMessage(ctx, &msg)
	if err != nil {
		reqLog.Errorfln("Failed to insert scheduled message from %s in %s into the database: %v", user.UserID, msg.RoomID, err)
		errInsertDatabase.Write(w)
		return
	}
	msg.UserAccessToken = user.AccessToken
	prepareIfScheduledSoon(ctx, &msg, false)
	err = json.NewEncoder(w).Encode(msg)
	if err != nil {
		reqLog.Warnfln("Failed to encode message %d in response to %s: %v", msg.ID, user.UserID, err)
	}
	reqLog.Debugfln("%s scheduled a message (ID %d) to be sent at %d in %s", msg.UserID, msg.ID, msg.TargetTimestamp, msg.RoomID)
}
