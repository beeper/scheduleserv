module gitlab.com/beeper/scheduleserv

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/prometheus/client_golang v1.11.0
	maunium.net/go/maulogger/v2 v2.2.5
)
