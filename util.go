package main

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	log "maunium.net/go/maulogger/v2"

	"gitlab.com/beeper/scheduleserv/model"
)

// MaxScheduleFuture is the maximum time into the future that messages can be scheduled for.
const MaxScheduleFuture = 365 * 24 * time.Hour

// MaxScheduleCount is the maximum number of messages a user can schedule per room.
const MaxScheduleCount = 100

var RequireLBUserID = len(os.Getenv("REQUIRE_LB_USER_ID")) > 0

func tsToTime(millis int64) time.Time {
	return time.Unix(0, millis*int64(time.Millisecond))
}

// validateTargetTimestamp makes sure that the schedule target timestamp is within limits. Error responses are handled
// internally, a return value of false means the handler should exit without doing anything else.
func validateTargetTimestamp(w http.ResponseWriter, ts int64) bool {
	targetTime := tsToTime(ts)
	if targetTime.Before(time.Now()) {
		errSchedulePast.Write(w)
	} else if targetTime.After(time.Now().Add(MaxScheduleFuture)) {
		errScheduleTooFar.Write(w)
	} else {
		return true
	}
	return false
}

func fieldCheck(w http.ResponseWriter, isPresent bool, field string) bool {
	if !isPresent {
		errFieldMissing(field).Write(w)
		return false
	}
	return true
}

func readUserIP(r *http.Request) string {
	var ip string
	if trustForwardHeaders {
		ip = r.Header.Get("X-Forwarded-For")
	}
	if ip == "" {
		ip = r.RemoteAddr
	}
	return ip
}

var baseReqLog = log.Sub("Req")
var reqIDCounter int64
var logContextKey = "fi.mau.scheduleserv.log"

func getContextLog(ctx context.Context) log.Logger {
	return ctx.Value(logContextKey).(log.Logger)
}

// CheckAccessToken validates the user's access token against the homeserver and ensures it's stored in the database.
func CheckAccessToken(w http.ResponseWriter, r *http.Request) (*model.User, context.Context, log.Logger) {
	reqID := atomic.AddInt64(&reqIDCounter, 1)
	reqLog := baseReqLog.Sub(strconv.FormatInt(reqID, 10))

	token := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")
	user := &model.User{AccessToken: token}
	resp, err := user.CheckAccessToken(r.Context(), client, homeserverURL.String())
	if err != nil {
		reqLog.Warnfln("Failed to check token in request from %s: %v", readUserIP(r), err)
		errCheckAccessToken.Write(w)
		return nil, nil, nil
	}
	if len(resp.UserID) == 0 {
		w.WriteHeader(resp.Status)
		_ = json.NewEncoder(w).Encode(&resp)
		reqLog.Debugfln("Rejecting request from %s due to invalid access token (%s)", readUserIP(r), resp.ErrCode)
		return nil, nil, nil
	}
	user.UserID = resp.UserID
	lbUserID := r.Header.Get("X-Beeper-User")
	if (len(lbUserID) > 0 || RequireLBUserID) && lbUserID != user.UserID {
		reqLog.Debugfln("Rejecting request from %s due to mismatching X-Beeper-User header (%s != %s)", readUserIP(r), lbUserID, user.UserID)
		errMismatchingUserID.Write(w)
		return nil, nil, nil
	}
	err = db.UpsertUser(r.Context(), user)
	if err != nil {
		reqLog.Errorfln("Failed to insert user %s into the database: %v", user.UserID, err)
		errUpdateUserDatabase.Write(w)
		return nil, nil, nil
	}
	reqLog.Debugfln("Received request from %s/%s", user.UserID, readUserIP(r))
	ctx := context.WithValue(r.Context(), logContextKey, reqLog)
	return user, ctx, reqLog
}

// parseJSON parses the JSON from a HTTP request into the given interface. Error responses are handled internally,
// a return value of false means the handler should exit without doing anything else.
func parseJSON(w http.ResponseWriter, r *http.Request, into interface{}) bool {
	err := json.NewDecoder(r.Body).Decode(into)
	if err != nil {
		errRequestNotJSON.Write(w)
		return false
	}
	return true
}
