package main

import (
	"context"
	"sync"
	"time"

	log "maunium.net/go/maulogger/v2"

	"gitlab.com/beeper/scheduleserv/model"
)

const LoopInterval = 5 * time.Minute

var nextLoop = time.Now()
var preparedMessages sync.Map
var cancelledSentinel = &struct{}{}
var sendLog = log.Sub("Send")

func MessageSendLoop(ctx context.Context, wg *sync.WaitGroup) {
	for {
		select {
		case <-time.After(nextLoop.Sub(time.Now())):
			nextLoop = time.Now().Add(LoopInterval)
			messages, err := db.FindUpcomingMessages(ctx, LoopInterval)
			if err != nil {
				sendLog.Errorln("Failed to get upcoming messages:", err)
			} else if len(messages) > 0 {
				sendLog.Debugfln("Got %d messages to send in the next %v", len(messages), LoopInterval)
				for _, msg := range messages {
					go prepareForSending(msg, false)
				}
			}
		case <-ctx.Done():
			wg.Done()
			return
		}
	}
}

func prepareIfScheduledSoon(ctx context.Context, message *model.ScheduledMessage, uncancel bool) {
	if nextLoop.Before(tsToTime(message.TargetTimestamp)) {
		if uncancel {
			// There's still time until the scheduled target time, so clear any "cancelled" flags.
			val, ok := preparedMessages.Load(message.ID)
			if ok && val == cancelledSentinel {
				getContextLog(ctx).Debugfln("Removing cancel flag of %d", message.ID)
				preparedMessages.Delete(message.ID)
			}
		}
		return
	}
	getContextLog(ctx).Debugfln("Preparing to send %d out-of-loop", message.ID)
	// This message was scheduled to be sent before the next loop, so prepare it for sending immediately.
	go prepareForSending(message, true)
}

func cancelPreparedSend(message *model.ScheduledMessage) bool {
	value, ok := preparedMessages.Load(message.ID)
	if !ok {
		// Not scheduled, safe to re-schedule.
		preparedMessages.Store(message.ID, cancelledSentinel)
		return true
	}
	if value == nil {
		// Value was set to nil, which means the message is being sent and can't be cancelled.
		return false
	}
	cancelFunc, ok := value.(context.CancelFunc)
	if ok {
		// Message is still sleeping, safe to re-schedule after cancelling.
		cancelFunc()
		preparedMessages.Store(message.ID, cancelledSentinel)
		sendLog.Debugfln("Cancelled prepared send of %d", message.ID)
		return true
	}
	// This shouldn't happen
	sendLog.Warnfln("Prepared message %d has unexpected status type %T", message.ID, value)
	return false
}

func prepareForSending(message *model.ScheduledMessage, evenIfCancelled bool) {
	ctx, cancel := context.WithCancel(context.Background())
	if val, alreadyScheduled := preparedMessages.LoadOrStore(message.ID, cancel); alreadyScheduled {
		if val == cancelledSentinel {
			if evenIfCancelled {
				sendLog.Debugfln("Removing cancel flag of %d (and replacing with sleep flag)", message.ID)
				preparedMessages.Store(message.ID, cancel)
			} else {
				sendLog.Debugfln("Skipping preparing %d as a previous send attempt was cancelled", message.ID)
				return
			}
		} else if val == nil {
			sendLog.Debugfln("Skipping preparing %d as it's already being sent", message.ID)
			return
		} else if _, ok := val.(context.CancelFunc); ok {
			sendLog.Debugfln("Skipping preparing %d as it's already prepared for sending", message.ID)
			return
		}
	}
	timeToSleep := tsToTime(message.TargetTimestamp).Sub(time.Now())
	sendLog.Debugfln("Sleeping %v to post %d", timeToSleep, message.ID)
	select {
	case <-time.After(timeToSleep):
	case <-ctx.Done():
	}
	if ctx.Err() != nil {
		sendLog.Debugfln("Sending %d was aborted by context: %v", message.ID, ctx.Err())
		return
	}
	preparedMessages.Store(message.ID, nil)
	defer preparedMessages.Delete(message.ID)
	sendLog.Debugfln("Sending %d (%s in %s by %s) to Matrix", message.ID, message.EventType, message.RoomID, message.UserID)
	err := message.Send(client, homeserverURL.String())
	if err != nil {
		sendLog.Errorfln("Failed to send %d: %v", message.ID, err)
		// TODO store a fake error in the database?
		return
	}
	sendLog.Debugfln("Got HTTP %d sending %d: %+v", message.ResponseStatus, message.ID, message.Response)
	err = db.UpdateMessageResponse(context.Background(), message)
	if err != nil {
		sendLog.Errorfln("Failed to store %d's send result: %v", message.ID, err)
	}
}
