package main

import (
	"context"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "maunium.net/go/maulogger/v2"

	"gitlab.com/beeper/scheduleserv/database"
)

var homeserverURL *url.URL
var db *database.Database
var client *http.Client
var trustForwardHeaders bool
var mainLog = log.Sub("Main")

func main() {
	log.DefaultLogger.TimeFormat = "Jan _2, 2006 15:04:05"
	if len(os.Getenv("DEBUG")) > 0 {
		log.DefaultLogger.PrintLevel = log.LevelDebug.Severity
	}

	client = &http.Client{
		Timeout: 3 * time.Minute,
	}

	trustForwardHeaders, _ = strconv.ParseBool(os.Getenv("TRUST_FORWARD_HEADERS"))

	var err error
	homeserverURL, err = url.Parse(os.Getenv("HOMESERVER_URL"))
	if err != nil {
		mainLog.Fatalln("Failed to parse homeserver URL:", err)
		os.Exit(10)
	}
	homeserverURL.Path = ""
	homeserverURL.RawPath = ""

	db, err = database.Connect()
	if err != nil {
		mainLog.Fatalln("Failed to connect to database:", err)
		os.Exit(11)
	}

	err = db.Upgrade()
	if err != nil {
		mainLog.Fatalln("Failed to update database schema:", err)
		os.Exit(12)
	}

	router := mux.NewRouter()
	router.HandleFunc("/_matrix/client/unstable/com.beeper.scheduleserv/schedule/{txnID}", ScheduleMessage).Methods(http.MethodPut)
	router.HandleFunc("/_matrix/client/unstable/com.beeper.scheduleserv/list/{roomID}", ListScheduledMessages).Methods(http.MethodGet)
	router.HandleFunc("/_matrix/client/unstable/com.beeper.scheduleserv/{id:[0-9]+}", GetScheduledMessage).Methods(http.MethodGet, http.MethodDelete, http.MethodPatch)
	router.Handle("/metrics", promhttp.Handler())
	addr := os.Getenv("LISTEN_ADDRESS")
	if len(addr) == 0 {
		addr = ":8080"
	}
	server := &http.Server{
		Addr:    addr,
		Handler: router,
	}
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		mainLog.Infoln("Starting to listen on", server.Addr)
		err = server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			mainLog.Fatalln("Error in listener:", err)
		}
		wg.Done()
	}()
	sendContext, cancelSend := context.WithCancel(context.Background())
	go MessageSendLoop(sendContext, &wg)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

	cancelSend()
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = server.Shutdown(ctx)
	if err != nil {
		mainLog.Errorln("Failed to close server:", err)
	}
	wg.Wait()
	mainLog.Infoln("Server stopped")
}
