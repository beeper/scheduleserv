package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/beeper/scheduleserv/model"
)

func GetScheduledMessage(w http.ResponseWriter, r *http.Request) {
	user, ctx, reqLog := CheckAccessToken(w, r)
	if user == nil {
		return
	}
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	msg, err := db.GetMessageByID(r.Context(), id)
	if err != nil {
		reqLog.Errorfln("Failed to get message %d from database (requested by %s): %v", id, user.UserID, err)
		errGetFromDatabase.Write(w)
		return
	}
	if msg == nil {
		reqLog.Debugfln("%s requested non-existent message %d", user.UserID, id)
		errNotFound.Write(w)
		return
	} else if msg.UserID != user.UserID {
		reqLog.Debugfln("%s requested message %d belonging to a different user (%s)", user.UserID, id, msg.UserID)
		errNotFound.Write(w)
		return
	}
	switch r.Method {
	case http.MethodPatch:
		msg.UserAccessToken = user.AccessToken
		if !PatchScheduledMessage(msg, w, r.WithContext(ctx)) {
			return
		}
		// Message patched, return the data like GET does
		fallthrough
	case http.MethodGet:
		err = json.NewEncoder(w).Encode(msg)
		if err != nil {
			reqLog.Warnfln("Failed to encode message %d in response to %s: %v", msg.ID, user.UserID, err)
		} else {
			reqLog.Debugfln("Responded to %s with message %d", user.UserID, id)
		}
	case http.MethodDelete:
		if !cancelPreparedSend(msg) {
			reqLog.Debugfln("%s attempted to unschedule message %d, but it's already sending", msg.UserID, msg.ID)
			errAlreadySending.Write(w)
			return
		}
		err = db.DeleteMessage(ctx, msg)
		if err != nil {
			reqLog.Errorfln("Failed to delete message %d as requested by %s: %v", msg.ID, user.UserID, err)
			errDeleteDatabase.Write(w)
		} else {
			if msg.ResponseStatus != 0 {
				reqLog.Debugfln("%s deleted sent message %d", msg.UserID, msg.ID)
			} else {
				reqLog.Debugfln("%s unscheduled %d", msg.UserID, msg.ID)
			}
			w.WriteHeader(http.StatusNoContent)
		}
	}
}

func PatchScheduledMessage(msg *model.ScheduledMessage, w http.ResponseWriter, r *http.Request) bool {
	ctx := r.Context()
	reqLog := getContextLog(ctx)
	if msg.ResponseStatus != 0 {
		reqLog.Debugfln("%s attempted to update message %d, but it was already sent", msg.UserID, msg.ID)
		errNoRescheduleSent.Write(w)
		return false
	}
	var updatedMessage model.ScheduledMessage
	if !parseJSON(w, r, &updatedMessage) {
		return false
	}
	if updatedMessage.TargetTimestamp != 0 {
		if !validateTargetTimestamp(w, updatedMessage.TargetTimestamp) {
			return false
		}
		msg.TargetTimestamp = updatedMessage.TargetTimestamp
		reqLog.Debugfln("%s requested rescheduling %d to %d", msg.UserID, msg.ID, msg.TargetTimestamp)
	}
	if updatedMessage.Content != nil {
		msg.Content = updatedMessage.Content
	}
	if !cancelPreparedSend(msg) {
		reqLog.Debugfln("%s attempted to update message %d, but it's already sending", msg.UserID, msg.ID)
		errAlreadySending.Write(w)
		return false
	}
	err := db.UpdateMessageTarget(ctx, msg)
	if err != nil {
		reqLog.Errorfln("Failed to update message %d as requested by %s: %v", msg.ID, msg.UserID, err)
		errUpdateDatabase.Write(w)
		return false
	}
	reqLog.Debugfln("%s successfully updated scheduled message %d", msg.UserID, msg.ID)
	prepareIfScheduledSoon(ctx, msg, true)
	return true
}
