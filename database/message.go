package database

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"

	"gitlab.com/beeper/scheduleserv/model"
)

func scanMessage(row pgx.Row) (*model.ScheduledMessage, error) {
	var msg model.ScheduledMessage
	err := row.Scan(
		&msg.ID, &msg.TxnID, &msg.RoomID, &msg.UserID, &msg.EventType, &msg.StateKey, &msg.Content, &msg.TargetTimestamp,
		&msg.SentTimestamp, &msg.ResponseStatus, &msg.Response, &msg.UserAccessToken,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to scan message row: %w", err)
	}
	return &msg, nil
}

func scanMessages(rows pgx.Rows) ([]*model.ScheduledMessage, error) {
	messages := make([]*model.ScheduledMessage, 0)
	for rows.Next() {
		msg, err := scanMessage(rows)
		if err != nil {
			return nil, err
		}
		messages = append(messages, msg)
	}
	return messages, nil
}

// FindUpcomingMessages finds all messages scheduled to be sent in the next given duration.
func (db *Database) FindUpcomingMessages(ctx context.Context, delta time.Duration) ([]*model.ScheduledMessage, error) {
	targetTimestamp := time.Now().Add(delta).UnixMilli()
	res, err := db.pool.Query(ctx, `
		SELECT id, txn_id, room_id, scheduled_messages.user_id, event_type, state_key, content,
			target_timestamp, sent_timestamp, response_status, response, users.access_token
		FROM scheduled_messages
		LEFT JOIN users ON scheduled_messages.user_id = users.user_id
		WHERE response_status=0 AND target_timestamp<$1
		ORDER BY target_timestamp
	`, targetTimestamp)
	if err != nil {
		return nil, fmt.Errorf("failed to query upcoming messages: %w", err)
	}
	return scanMessages(res)
}

// FindMessagesForUserInRoom returns all messages that the given user has scheduled in the given room.
func (db *Database) FindMessagesForUserInRoom(ctx context.Context, userID, roomID string) ([]*model.ScheduledMessage, error) {
	res, err := db.pool.Query(ctx, `
		SELECT id, txn_id, room_id, user_id, event_type, state_key, content, target_timestamp, sent_timestamp, response_status, response, ''
		FROM scheduled_messages
		WHERE user_id=$1 AND room_id=$2 AND (sent_timestamp>$3 OR response_status<>200)
		ORDER BY target_timestamp
	`, userID, roomID, time.Now().Add(-24*time.Hour).UnixMilli())
	if err != nil {
		return nil, fmt.Errorf("failed to query messages for user in room: %w", err)
	}
	return scanMessages(res)
}

func (db *Database) CountMessagesForUserInRoom(ctx context.Context, userID, roomID string) (count int, err error) {
	err = db.pool.QueryRow(ctx, `
		SELECT COUNT(*) FROM scheduled_messages WHERE user_id=$1 AND room_id=$2 AND response_status=0
	`, userID, roomID).Scan(&count)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		return 0, fmt.Errorf("failed to query number of messages for user in room: %w", err)
	}
	return count, nil
}

func (db *Database) GetMessageByTxnID(ctx context.Context, txnID, userID, roomID, eventType string) (*model.ScheduledMessage, error) {
	res := db.pool.QueryRow(ctx, `
		SELECT id, txn_id, room_id, user_id, event_type, state_key, content, target_timestamp, sent_timestamp, response_status, response, ''
		FROM scheduled_messages WHERE txn_id=$1 AND user_id=$2 AND room_id=$3 AND event_type=$4
	`, txnID, userID, roomID, eventType)
	return scanMessage(res)
}

func (db *Database) GetMessageByID(ctx context.Context, id int) (*model.ScheduledMessage, error) {
	res := db.pool.QueryRow(ctx, `
		SELECT id, txn_id, room_id, user_id, event_type, state_key, content, target_timestamp, sent_timestamp, response_status, response, ''
		FROM scheduled_messages WHERE id=$1
	`, id)
	return scanMessage(res)
}

const insertQuery = `
	INSERT INTO scheduled_messages (txn_id, room_id, user_id, event_type, state_key, content, target_timestamp)
	VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id
`

// InsertMessage stores the given message to be sent in the future.
func (db *Database) InsertMessage(ctx context.Context, message *model.ScheduledMessage) error {
	err := db.pool.QueryRow(ctx, insertQuery,
		message.TxnID, message.RoomID, message.UserID,
		message.EventType, message.StateKey, message.Content, message.TargetTimestamp,
	).Scan(&message.ID)
	if err != nil {
		return fmt.Errorf("failed to insert message into database: %w", err)
	}
	return nil
}

func (db *Database) UpdateMessageTarget(ctx context.Context, message *model.ScheduledMessage) error {
	_, err := db.pool.Exec(ctx,
		"UPDATE scheduled_messages SET target_timestamp=$2, content=$3 WHERE id=$1",
		message.ID, message.TargetTimestamp, message.Content)
	if err != nil {
		return fmt.Errorf("failed to update message target in database: %w", err)
	}
	return nil
}

// UpdateMessageResponse updates the given scheduled message with the response data and stores the current timestamp as the send timestamp.
func (db *Database) UpdateMessageResponse(ctx context.Context, message *model.ScheduledMessage) error {
	sentMs := time.Now().UnixMilli()
	message.SentTimestamp = &sentMs
	_, err := db.pool.Exec(ctx,
		"UPDATE scheduled_messages SET sent_timestamp=$2, response_status=$3, response=$4 WHERE id=$1",
		message.ID, message.SentTimestamp, message.ResponseStatus, message.Response)
	if err != nil {
		return fmt.Errorf("failed to update message send result in database: %w", err)
	}
	return nil
}

// DeleteMessage deletes the scheduled message with the given ID.
func (db *Database) DeleteMessage(ctx context.Context, message *model.ScheduledMessage) error {
	_, err := db.pool.Exec(ctx, "DELETE FROM scheduled_messages WHERE id=$1", message.ID)
	if err != nil {
		return fmt.Errorf("failed to delete scheduled message: %w", err)
	}
	return nil
}

// DeleteSuccessfulMessages deletes all successful scheduled messages that were sent to Matrix longer than the given duration ago.
func (db *Database) DeleteSuccessfulMessages(ctx context.Context, delta time.Duration) error {
	sentBefore := time.Now().Add(-delta).UnixMilli()
	_, err := db.pool.Exec(ctx, "DELETE FROM scheduled_messages WHERE response_status=200 AND sent_timestamp<$1", sentBefore)
	if err != nil {
		return fmt.Errorf("failed to delete old successfully sent messages: %w", err)
	}
	return nil
}
