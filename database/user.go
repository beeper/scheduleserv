package database

import (
	"context"
	"fmt"

	"gitlab.com/beeper/scheduleserv/model"
)

func (db *Database) UpsertUser(ctx context.Context, user *model.User) error {
	_, err := db.pool.Exec(ctx, `
		INSERT INTO users (user_id, access_token) VALUES ($1, $2)
		ON CONFLICT (user_id) DO UPDATE SET access_token=excluded.access_token
	`, user.UserID, user.AccessToken)
	if err != nil {
		return fmt.Errorf("failed to insert user into database: %w", err)
	}
	return err
}
