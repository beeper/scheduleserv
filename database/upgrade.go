package database

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4"
)

type upgrade struct {
	Message string
	Func    func(conn pgx.Tx) error
}

var upgrades = []upgrade{{
	"Initial version",
	func(conn pgx.Tx) error {
		_, err := conn.Exec(context.Background(), `
			CREATE TABLE users (
				user_id      TEXT PRIMARY KEY,
				access_token TEXT NOT NULL
			);
		`)
		if err != nil {
			return err
		}
		_, err = conn.Exec(context.Background(), `
			CREATE TABLE scheduled_messages (
				id     BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
				txn_id TEXT   NOT NULL,

				room_id    TEXT NOT NULL,
				user_id    TEXT NOT NULL REFERENCES users(user_id),
				event_type TEXT NOT NULL,
				state_key  TEXT,
				content    jsonb NOT NULL,

				target_timestamp BIGINT  NOT NULL,
				sent_timestamp   BIGINT,
				response_status  INTEGER NOT NULL DEFAULT 0,
				response         jsonb,

				UNIQUE (txn_id, room_id, user_id, event_type)
			);
		`)
		return err
	},
}}

func setVersionWrapper(upgrader func(conn pgx.Tx) error, version int) func(conn pgx.Tx) error {
	return func(conn pgx.Tx) error {
		err := upgrader(conn)
		if err != nil {
			return err
		}
		_, err = conn.Exec(context.Background(), "DELETE FROM version")
		if err != nil {
			return fmt.Errorf("failed to delete current version row: %w", err)
		}
		_, err = conn.Exec(context.Background(), "INSERT INTO version VALUES ($1)", version)
		if err != nil {
			return fmt.Errorf("failed to insert new version row: %w", err)
		}
		return nil
	}
}

// Upgrade updates the database schema to the latest version.
func (db *Database) Upgrade() error {
	ctx := context.Background()
	conn, err := db.pool.Acquire(ctx)
	if err != nil {
		return fmt.Errorf("failed to acquire connection to upgrade db: %w", err)
	}
	_, err = conn.Exec(ctx, "CREATE TABLE IF NOT EXISTS version (version INTEGER PRIMARY KEY)")
	if err != nil {
		return fmt.Errorf("failed to ensure version table exists: %w", err)
	}
	var version int
	err = conn.QueryRow(ctx, "SELECT version FROM version").Scan(&version)
	if err != nil && err != pgx.ErrNoRows {
		return fmt.Errorf("failed to get current database schema version: %w", err)
	}

	if len(upgrades) > version {
		for index, upgrade := range upgrades[version:] {
			newVersion := version + index + 1
			dbLog.Infofln("Updating database schema to v%d: %s", newVersion, upgrade.Message)
			err = conn.BeginFunc(ctx, setVersionWrapper(upgrade.Func, newVersion))
			if err != nil {
				return fmt.Errorf("failed to upgrade database schema to v%d: %w", newVersion, err)
			}
		}
		dbLog.Infofln("Database schema update to v%d", len(upgrades))
	}
	return nil
}
