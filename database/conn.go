package database

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
	log "maunium.net/go/maulogger/v2"
)

type Database struct {
	pool *pgxpool.Pool
}

var dbLog = log.Sub("Database")

// Connect creates a new pgx connection pool.
func Connect() (*Database, error) {
	var db Database
	var err error
	db.pool, err = pgxpool.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	return &db, err
}
